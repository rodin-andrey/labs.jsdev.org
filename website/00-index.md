---
layout: page
title: JavaScript Development with modern Technologies
permalink: /
---

# JavaScript Development with modern Technologies


<br/>

### Plans

* ReactJs 2018, the complete guide with real world projects
* [Lynda.com / Emmanuel Henri] Learning the Elastic Stack [2018, ENG]
* [Udemy, Traversy Brad] React Front To Back [2018, ENG]
* Advanced React and Redux: 2018 Edition
* Server Side Rendering with React and Redux
* [Lynda] Node.js: Design Patterns [2018, ENG]

<br/>

**and maybe someday**

<br/>

* <a href="https://github.com/planetoftheweb/learnangular5" rel="nofollow">~~[Lynda, Ray Villalobos] Learning Angular [2018, ENG]~~</a> 
* Blockchain Development With NodeJS (https://www.youtube.com/watch?v=av9HRXEVDWc)
* Learn Angular 5 in 30 minutes Eduonix (https://www.youtube.com/watch?v=_tEZgpmxKck)
* Machine Learning Tutorial for Beginners - USING JAVASCRIPT! (https://www.youtube.com/watch?v=9Hz3P1VgLz4&t=0s)

<br/>

**and watch again**

<br/>

* [Lynda] Learning Full-Stack JavaScript Development: MongoDB, Node and React [2016, ENG]
* Creating Apps With Angular, Node, and Token Authentication [2014, ENG]
* Some parts from: [udemy Andrew Mead] The Complete Node.js Developer Course 2.0 (updated) [2016, ENG] (MongoDB, Security, Async)
* Docker for Web Developers [2016, ENG]





<br/>

### Oracle MOOC: Soar higher with Oracle JavaScript Extension Toolkit (JET) 4.0 (2018)


<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/jiZh6ERmRkY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>


<a href="https://apexapps.oracle.com/pls/apex/f?p=44785:149:0::::P149_EVENT_ID:5725">Link</a>



<br/>

### Oracle MOOC: Introduction to NodeJS Using Oracle Cloud (2018)


<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/vNzuEYxsVyg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<br/>

**This video course can help to start working with node.js with oracle db**  

<a href="/backend/nodejs/2018/introduction-to-nodejs-using-oracle-cloud/">Link</a>
