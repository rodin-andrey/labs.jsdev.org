---
layout: page
title: The Complete Node.js Developer Course 2.0 (updated)
permalink: /backend/nodejs/2016/the-complete-nodejs-developer-course-2/prepare-environment/
---

# The Complete Node.js Developer Course 2.0 (updated) [2016, ENG]

<br/>

## Prepare Environment for Development


<br/>

    $ project_name=the-complete-nodejs-developer-course-2
    $ echo $project_name
    the-complete-nodejs-developer-course-2

<br/>

    $ project_folder=~/projects/dev/js/nodejs/
    $ echo $project_folder
    $ mkdir -p ${project_folder}/${project_name}

<br/>

    $ docker run -it \
    -p 80:8080 -p 1337:1337 -p 3000:3000 -p 4000:4000 -p 5000:5000 -p 6000:6000 -p 7000:7000 -p 8000:8000 -p 9000:9000 \
    --name ${project_name} \
    -v ${project_folder}/${project_name}:/project \
    node \
    /bin/bash


<br/>

    # apt-get update
    # apt-get install -y vim git

<br/>

    # mkdir -p /usr/local/lib/node_modules/

<br/>

    # username=developer

    # adduser --disabled-password --gecos "" ${username}

    # chown -R ${username} /project/

    # su - ${username}

        
<br/>

    $  vi ~/.bashrc

<br/>    

In the bottom I am add

    ###############################
    # USER DEFINED
    . ~/.bash_profile
    ###############################

<br/>
    
    $ echo "export PS1='$ '" >> ~/.bash_profile
    $ echo 'export PATH=$PATH:./node_modules/.bin' >> ~/.bash_profile
    
    $ . ~/.bash_profile
