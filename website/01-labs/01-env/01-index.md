---
layout: page
title: JavaScript Development Environment
permalink: /env/
---

# JavaScript Development Environment

<br/>

<a href="/env/docker/">Docker</a> || <a href="/env/atom/">Atom</a> || <a href="/env/git/">Git</a>

<br/>

I'm working on ubuntu linux 14.04 LTS with Docker and my IDE is Atom with some plugins. No more anything need for comfort development.


<br/><br/>


For develop app in the browser, without anything else, you can use c9.io service.


<br/>

### Video Courses about environment:

<br/>

**[Pluralsight] Building a JavaScript Development Environment**

https://github.com/coryhouse/javascript-development-environment

<br/>

And additionally. I very interesting in CoreOS as platform for docker container. As distributed platform for multiple docker containers. For study CoreOS, I watched course:


**[O'Reilly Media / Infinite Skills] Introduction to CoreOS Training Video [2015, ENG]**
