---
layout: page
title: React.js
permalink: /frontend/react/
---

# React.js

<ul>
    <li><a href="/frontend/react/the-complete-react-web-developer-course-2nd-edition/">[Udemy] The Complete React Web Developer Course (2nd Edition) [2017, ENG]</a></li>
    <li><a href="https://bitbucket.org/marley-react/the-complete-react-web-app-developer-course">[Udemy] The Complete React Web App Developer Course [2016, ENG]</a></li>
    <li><a href="https://bitbucket.org/marley-react/reactjs-and-flux-learn-by-building-10-projects">[Eduonix] ReactJS and Flux: Learn By Building 10 Projects [ENG, 2016]</a></li>
</ul>


<br/>

### Project in React Native | Book Finder App


<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/ydjdaHDT-sg" frameborder="0" allowfullscreen></iframe>

</div>


<br/>

### Learn How to Setup a Development Environment for Node.js and React


<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLLnpHn493BHFfs3Uj5tvx17mXk4B4ws4p" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### React For Beginners
https://bitbucket.org/marley-react/react-for-beginners-starter-files


<br/>

### React JS Tutorials
https://www.youtube.com/playlist?list=PLoYCgNOIyGABj2GQSlDRjgvXtqfDxKm5b


<br/>

### Learning React.js [1] - An Overview
https://www.youtube.com/watch?v=vYldnghykaU
