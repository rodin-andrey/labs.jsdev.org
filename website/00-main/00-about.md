---
layout: page
title: About labs.jsdev.org
permalink: /about/
---

# About labs.jsdev.org

<br/>

### I'm planning to study javascript with modern technologies. And results of my study I'm planning to share here


<br/>

### If you can recommend interesting video courses or books for me, please do it. Or if you will watch video courses or read books and agree to share results, please write on email.

Much more interesting if all of us will use docker (or other containers) to prepare applications for publishing by 2-3 clicks.

I have an example with coreos, angular, nodejs, rethinkdb. If someone will interesting, i will share example. But this example is not so simple.

<br/>

![Marley](http://img.fotografii.org/a3333333mail.gif "Marley")

<br/>

<a href="https://gitter.im/jsdev-org/Lobby" rel="nofollow"><img src="https://badges.gitter.im/jsdev-org/Lobby.svg" alt="jsdev chat room"></a>
<a href="https://travis-ci.org/jsdev-org/labs.jsdev.org" rel="nofollow"><img src="https://travis-ci.org/jsdev-org/labs.jsdev.org.svg?branch=gh-pages" alt="jsdev build status"></a>
<a href="http://isitmaintained.com/project/jsdev-org/labs.jsdev.org" rel="nofollow"><img src="http://isitmaintained.com/badge/open/jsdev-org/labs.jsdev.org.svg" alt="open issue"></a>


<br/>

### I'm trying to study Java Script frameworks and want to be a Java Script developer too


Now I'm working on position Oracle ADF Developer (That is a Java Framework with JSF Technologies). And we develop applications like <a href="https://www.youtube.com/watch?v=79QQbQ-PDkM" rel="nofollow">this</a>.


<br/>

### And I want to improve my English. And if it possible, please correct my mistakes. I will appreciate it.


<br/>

### Currently I'm working on:


<ul>
    <li><a href="/backend/nodejs/2016/learning-full-stack-javascript-development/">Learning Full-Stack JavaScript Development: MongoDB, Node and React</a></li>
    <li><a href="/frontend/react/the-complete-react-web-developer-course-2nd-edition/">[Udemy / Andrew Mead] The Complete React Web Developer Course (2nd Edition) [2017, ENG]</a></li>
    <!-- <li><a href="/backend/nodejs/2016/the-complete-nodejs-developer-course-2/">[Udemy / Andrew Mead] The Complete Node.js Developer Course 2.0 (updated) [2016, ENG]</a></li> -->
</ul>

<br/>

After I will finish it.

I can start study:

(So many materials. I can't find enough time)

<ul>

    <li>[es6.io Wes Bos] ES6 For Everyone! [2016, ENG]</li>
    <li>[Pluralsight.com Wes Higbee] Modern Asynchronous JavaScript [2016, ENG]</li>
    <li><a href="/frontend/angular/4/brad-traversy-angular-4-front-to-back/">[Udemy] Brad Traversy - Angular 4 Front To Back [2017, ENG]</a></li>
    <li>[Udemy] Learn Angular 2 from Beginner to Advanced [2017, ENG]</li>


    <li>Courses about REST</li>
    <li>Courses about SECURITY</li>
    <li>[Udemy] Angular 4 - The Complete Guide [2017, ENG]</li>
    <li>[Udemy] ES6 Javascript: The Complete Developer's Guide [2016, ENG]</li>
    <li><a href="https://bitbucket.org/marley-nodejs/express-mongodb-build-a-shopping-cart" rel="nofollow">[Mindspace, Youtube] NodeJS / Express / MongoDB - Build a Shopping Cart [ENG, 2016]</a></li>
</ul>


<br/>

### And may be later:

<br/>

<ul>
    <li>[Udemy] Angular 2 - The Complete Guide (Updated to final version) [2016, ENG]</li>
    <li>[Pluralsight] Getting Started with Reactive Programming Using RxJS [2016, ENG]</li>
    <li><a href="https://www.youtube.com/watch?v=Wm6CUkswsNw" rel="nofollow">Build An HTML5 Website With A Responsive Layout</a></li>
    <li><a href="https://www.youtube.com/playlist?list=PLLnpHn493BHGGHRmdXip3GswwLWY9S6of" rel="nofollow">Meteor 1.4 + React For Everyone</a></li>
    <li><a href="https://www.youtube.com/watch?v=pB7EwxwSfVk" rel="nofollow">Custom Bootstrap Theme With Sass</a></li>
    <li><a href="https://www.youtube.com/watch?v=nBUT3TQeggo" rel="nofollow">API development in Node, Express, ES6 and MongoDB</a></li>
    <li><a href="https://www.youtube.com/watch?v=A71aqufiNtQ" rel="nofollow">React JS Crash Course</a></li>
    <li><a href="https://www.youtube.com/watch?v=7MLXuG83Fsw" rel="nofollow">Intro To Memcached</a></li>
    <li><a href="https://www.youtube.com/playlist?list=PL55RiY5tL51rLeDxRPlE6Hsnss3QDCCKc" rel="nofollow">Angular 2.0 Final - Getting Started</a></li>
    <li><a href="https://www.youtube.com/playlist?list=PL55RiY5tL51rrC3sh8qLiYHqUV3twEYU_" rel="nofollow">ReactJS + Redux Basics</a></li>
    <li><a href="https://www.youtube.com/playlist?list=PL55RiY5tL51oyA8euSROLjMFZbXaV7skS" rel="nofollow">ReactJS Basics</a></li>
    <li><a href="https://bitbucket.org/marley-angular/ng2-camp-example" rel="nofollow">Using Redux with Angular 2 (AngularJS TO - July 2016)</a></li>
    <li><a href="https://www.youtube.com/watch?v=-3vvxn78MH4&list=PL55RiY5tL51rajp7Xr_zk-fCFtzdlGKUp&index=2" rel="nofollow">NodeJS / Express / MongoDB - Build a Shopping Cart - #1 Intro & Setup</a></li>
    <li><a href="https://www.youtube.com/watch?v=ei7FsoXKPl0" rel="nofollow">RxJS Observables Crash Course</a></li>
    <li>[Udemy] The Complete React Web App Developer Course [2016, ENG]</li>
    <li><a href="https://www.udemy.com/angular-2-crash-course/" rel="nofollow">Angular 2 Crash Course with TypeScript</a> (vids not found)</li>
    <li><a href="https://www.youtube.com/watch?v=tMEyrfjoWEs" rel="nofollow">AngularJS 2 and NodeJS: The Practical Guide to MEAN Stack 2.0</a></li>
    <li><a href="https://www.youtube.com/watch?v=viaF0hM8G94" rel="nofollow">ES6 Promises (with Angular 2) Explained</a></li>
    <li><a href="https://www.youtube.com/watch?v=PFP0oXNNveg" rel="nofollow">MEAN App From Scratch - MongoDB, Angular 2, Express, Node.js</a> (same project as in course Learn Angular 2 Development By Building 12 Apps) </li>
    <li><a href="https://www.youtube.com/watch?v=ei7FsoXKPl0" rel="nofollow">RxJS Observables Crash Course</a></li>
    <li><a href="https://www.youtube.com/watch?v=i_dHFvi1BJc" rel="nofollow">Angular 2 Auth0 Application</a></li>
    <li><a href="https://github.com/DeborahK/Angular2-GettingStarted" rel="nofollow">Angular 2: Getting Started</a></li>
    <li>Learn ExpressJs building 10 projects</li>
    <li>Learn MeteorJS By Building 10 Real World Projects</li>
    <li>Projects in MongoDB Learn MongoDB Building Ten Projects</li>
    <li>[Frontend Masters] Rethinking Asynchronous JavaScript [2016, ENG]</li>
</ul>
