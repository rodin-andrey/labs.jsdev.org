---
layout: page
title: Web Developer Roadmap 2017
permalink: /web-developer-roadmap-2017/
---


# Web Developer Roadmap 2017

<br/>

### Introduction

<br/>

<div align="center">
    <img src="https://camo.githubusercontent.com/2e6d200ca77e11ad11721c4cbff86104bd005eac/68747470733a2f2f692e696d6775722e636f6d2f71426c5436374e2e706e67" alt="Introduction" />
</div>


<br/>

### Front-end Roadmap

<br/>


<div align="center">
    <img src="https://camo.githubusercontent.com/93280354d6367052b6dbb71bbcd76c2ea81294c8/68747470733a2f2f692e696d6775722e636f6d2f3576465457634f2e706e67" alt="Front-end Roadmap" />
</div>


<br/>

### Back-end Roadmap

<br/>


<div align="center">
    <img src="https://camo.githubusercontent.com/a69353cebac96bd2e82b45771d6edd32715ca0c3/68747470733a2f2f692e696d6775722e636f6d2f6d3956385a69562e706e67" alt="Back-end Roadmap" />
</div>


<br/>

### DevOps Roadmap

<br/>


<div align="center">
    <img src="https://camo.githubusercontent.com/3e4577550f330f8b507d7aff61d09c0fadd7d93f/687474703a2f2f692e696d6775722e636f6d2f694e4e495a7a542e706e67" alt="DevOps Roadmap">
</div>


<br/>
<hr/>
<br/>

**More inforation:**  
https://github.com/kamranahmedse/developer-roadmap
