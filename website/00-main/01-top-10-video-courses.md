---
layout: page
title: Top 10 Video Courses for javascript (frameworks) development
permalink: /top-10-video-courses/
---


# Top 10 Video Courses for javascript development by Frameworks

<br/>

My (Marley) opinion!

<br/>

### Docker

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/docker-for-web-developers/"><strong>1) Docker for Web Developers</strong></a> (Everyone should see)</li>
</ul>


<br/>

### Build Automation

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/javascript-build-automation-with-gulp.js/"><strong>2) JavaScript Build Automation With Gulp.js</strong></a> (Everyone should see)</li>
</ul>


<br/>

### Node.js

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/learn-nodejs-by-building-10-projects">3) [Eduonix] Learn Nodejs by building 10 projects [2015, ENG]</a></li>
</ul>



<br/>

### Angular

<ul>
    <li><a href="https://bitbucket.org/marley-angular/angular-2-from-the-ground-up">4) [Udemy] Angular 2 From The Ground Up [2016, ENG]</a> (Best Video For Starting in October 2016) </li>
    <li><a href="https://bitbucket.org/marley-angular/learn-angular-2-development-by-building-12-apps">5) [Eduonix] Learn Angular 2 Development By Building 12 Apps</a></li>
</ul>


<br/>

### Angular.js

<ul>
    <li><a href="https://bitbucket.org/marley-angular/learn-angular.js-by-building-10-projects">6) [Eduonix] Projects in AngularJS - Learn Angular.js by building 10 Projects [ENG, 2015]</a></li>
    <li><a href="https://bitbucket.org/marley-angular/angular.js-line-of-business-applications">7) AngularJS Line of Business Applications</a></li>
    <li><a href="https://bitbucket.org/marley-nodejs/creating-apps-with-angular-node-and-token-authentication">8) Creating-Apps-With-Angular-Node-and-Token-Authentication</a></li>

</ul>


<br/>

### React

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/mern-stack-front-to-back/">9) [Udemy] MERN Stack Front To Back: Full Stack React, Redux & Node.js [2018, ENG]</a></li>
</ul>


<br/>

### Testing

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/javascript-unit-testing-with-mocha-chai-and-sinon">10) [TutsPlus] JavaScript Unit Testing with Mocha, Chai and Sinon with Jason Rhodes [ENG, 2014]</a></li>
</ul>


<br/>
<br/>

Stupid bitch Kelly Manley From PiracySolution, <a href="https://bitbucket.org/marley-nodejs/kelly-manley-from-piracysolution-stupid-bitch" target="_blank">blocked my repos</a> what I typed by myself.

And now I do not recommend to by this course. Try to find it on the internet for free. It simply.


<br/>

**Also liked:**

<ul>

    <li>[Udemy] The Complete Node.js Developer Course 2.0.</li> I think It is most interesting modern video course for study node.js for beginners

    <li><a href="https://github.com/sysadm-ru/Introduction_To_CoreOS"> [O'Reilly] Introduction to CoreOS</a></li> In this course we made cluster with nodejs / rethinkdb application by coreos, docker.

    <li><a href="https://bitbucket.org/marley-angular/angular-2-with-webpack-project-setup"> [YouTube] Angular 2 with Webpack Project Setup</a></li>

    <li><a href="https://bitbucket.org/marley-nodejs/developing-for-the-mean-stack-and-mongodb">Developing-for-the-MEAN-Stack-and-MongoDB</a></li>

</ul>
